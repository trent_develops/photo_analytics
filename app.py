from chalice import Chalice, IAMAuthorizer
import requests
import os
import json
import boto3
import uuid
import base64

from decimal import Decimal
import datetime

app = Chalice(app_name='photo_analytiv=cs')
authorizer = IAMAuthorizer()

FACE_API_URI = 'https://westus2.api.cognitive.microsoft.com/face/v1.0/detect'
ENCRYPTED_AZURE_KEY = os.environ['encrypted_azure_key']
DYNAMODB_TABLE = os.environ['index_table']
BUCKET_NAME = 'photo-bucket-trent'    # TODO: move this to environment variable for flexibility
PREFIX = 'upload'  # TODO: move this to environment variable for flexibility

dynamodb = boto3.resource('dynamodb')


@app.route("/catalog", methods=['POST'], authorizer=authorizer)
def api_handler():
    # TODO: parse data from event (apiGatway->lambda events are not the same as s3->lambda events)
    request = app.current_request
    request_body = request.json_body
    imageUrl = request_body['imageUrl']

    # make api request
    r = call_face_api(imageUrl)

    # index image with data into dynamodb
    return index_data(r, imageUrl)


@app.on_s3_event(bucket=BUCKET_NAME, events=['s3:ObjectCreated:*'], prefix=PREFIX)
def handler(event):
    print("Object uploaded in bucket: %s, key: %s" % (event.bucket, event.key))
    # get https url for object in s3 bucket (make publicly accessible if needed)
    # Example: https://mybucket.s3.amazonaws.com/myfolder/afile.jpg
    imageUrl = 'https://{bucket}.s3.amazonaws.com/{key}'.format(
        bucket=event.bucket, key=event.key)
    print('ImageURL: {}'.format(imageUrl))

    # make api request
    r = call_face_api(imageUrl)

    # index image with data into dynamodb
    index_data(r, imageUrl)

# helper functions


def index_data(response, imageUrl):
    # generate imageId
    imageId = str(uuid.uuid4())

    # connect to db
    table = dynamodb.Table(DYNAMODB_TABLE)

    print('Indexing imageId {} into DynamoDB table {}'.format(
        imageId, DYNAMODB_TABLE))

    # setup payload and send to table
    payload = {'imageId': imageId, 'imageUrl': imageUrl}
    payload['faces'] = dumps(response)
    table.put_item(Item=payload)

    print('Indexing complete!')
    return payload


def call_face_api(imageUrl):

    # decrypt subscription key
    decrypted_key = decrypt_azure_key_with_kms_key(ENCRYPTED_AZURE_KEY)

    # build request to microsoft face api
    headers = {'Ocp-Apim-Subscription-Key': decrypted_key}
    params = {
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,' +
        'emotion,hair,makeup,occlusion,accessories,blur,exposure,noise'
    }
    payload = {'url': imageUrl}

    # make request
    response = requests.post(FACE_API_URI, params=params,
                             headers=headers, json=payload)
    response_json = response.json()
    print(response_json)

    # verify successful response
    response.raise_for_status()

    return response_json



def decrypt_azure_key_with_kms_key(encrypted_azure_key):
    # decrypt sub key
    kms = boto3.client('kms')
    response = kms.decrypt(CiphertextBlob=base64.b64decode(
        encrypted_azure_key))    # decode key before crypting it
    # decrypted key comes in bytes literal form, must decode to utf-8
    return response['Plaintext'].decode('utf-8')

# json dump fix functions


def decode_object_hook(dct):
    for key, val in dct.items():
        if isinstance(val, float):
            dct[key] = Decimal(str(val))
    return dct


def json_serial(val):
    if isinstance(val, datetime):
        serial = val.strftime('%Y-%m-%dT%H:%M:%S.%f')
        return serial
    elif isinstance(val, set):
        serial = list(val)
        return serial
    elif isinstance(val, uuid.UUID):
        serial = str(val.hex)
        return serial


def dumps(dct, *args, **kwargs):
    kwargs['object_hook'] = decode_object_hook
    return json.loads(json.dumps(dct, default=json_serial), *args, **kwargs)
