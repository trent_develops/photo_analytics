import json

incoming_event_from_s3 = {
  'Records':[
    {
      's3':{
        'bucket':{
          'name':'photo-bucket-trent'
        },
        'object':{
          'key':'upload/Sadie_and_Kelly.jpg'
        }
      }
    }
  ]
}

incoming_event_from_gateway = {
    "body": {'imageUrl':'https://photo-bucket-trent.s3.amazonaws.com/upload/Sadie_and_Kelly.jpg'}
}
