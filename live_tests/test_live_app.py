from unittest import TestCase
import app
from live_tests import livetestdata
import boto3
import mock

# This live test is to support debugging the application as it runs against live AWS resources.
# Environment variables must be set in the __init__.py of this shared directory.

class TestApp(TestCase):

    def setUp(self):
        pass

    def test_handler_happy_path(self):
        # setup data to send to function
        event = livetestdata.incoming_event_from_s3

        # call the function to test
        app.handler(event, 'context') # ignore pylint error E1121 on app object here

    # def test_api_handler_happy_path(self):
    #     # setup data to send to function
    #     event = livetestdata.incoming_event_from_gateway

    #     # call the function to test
    #     app.api_handler(event, 'context') # ignore pylint error E1121 on app object here
