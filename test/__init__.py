import os

# setup dummy environment variables
os.environ['encrypted_azure_key'] = 'dummy_sub_key'
os.environ['index_table'] = 'dummy_table_name'