from unittest import TestCase
import app
import test.testdata as testdata
import boto3
import mock

class TestApp(TestCase):

    def setUp(self):
        pass

    @mock.patch('app.dynamodb')
    def test_index_data_happy_path(self, dynamoMock):

        # set up mocks
        tableMock = mock.Mock()
        tableMock.put_item.return_value = {}
        dynamoMock.Table.return_value = tableMock

        # setup fake data to send to function
        face_api_response = testdata.mock_response_from_face_api
        imageurl = 'https://garbageurl.com/upload/nothing.jpg'

        # call the function to test
        app.index_data(face_api_response, imageurl)

        # verifications
        tableMock.put_item.assert_called_once()

    @mock.patch('app.dynamodb')
    def test_index_data_put_item_fails(self, dynamoMock):

        # set up expected data
        expected_exception_message = 'Put item failed'

        # set up mocks
        tableMock = mock.Mock()
        tableMock.put_item.side_effect = Exception(expected_exception_message)
        dynamoMock.Table.return_value = tableMock

        # setup fake data to send to function
        face_api_response = testdata.mock_response_from_face_api
        imageurl = 'https://garbageurl.com/upload/nothing.jpg'

        # call the function to test w/ verifications
        with self.assertRaises(Exception) as e:
            app.index_data(face_api_response, imageurl)

        # verifications
        self.assertEqual(expected_exception_message, e.exception.args[0]) # exception arguments are stored in args

    @mock.patch('requests.post')
    @mock.patch('app.decrypt_azure_key_with_kms_key')
    def test_call_face_api_happy_path(self, mock_decrypt_function, mock_requests_post):

        # set up mocks
        mock_response = mock.Mock()
        mock_response.json.return_value = testdata.mock_response_from_face_api
        mock_requests_post.return_value = mock_response
        mock_decrypt_function.return_value = 'dummystring'

        # setup fake data to send to function
        imageurl = 'https://garbageurl.com/upload/nothing.jpg'

        # call the function to test
        app.call_face_api(imageurl)

        # verifications
        mock_response.json.assert_called_once()

    @mock.patch('requests.post')
    @mock.patch('app.decrypt_azure_key_with_kms_key')
    def test_call_face_api_decrypt_fails(self, mock_decrypt_function, mock_requests_post):

        # expected data
        expected_exception_message = 'Something bad happened in this function.'

        # set up mocks
        mock_response = mock.Mock()
        mock_response.json.return_value = testdata.mock_response_from_face_api
        mock_requests_post.return_value = mock_response
        mock_decrypt_function.return_value = 'dummystring'
        mock_decrypt_function.side_effect = Exception('Something bad happened in this function.')

        # setup fake data to send to function
        imageurl = 'https://garbageurl.com/upload/nothing.jpg'

        # call the function to test
        with self.assertRaises(Exception) as e:
            app.call_face_api(imageurl)

        # verifications
        self.assertEqual(expected_exception_message, e.exception.args[0]) # exception arguments are stored in args
        mock_response.json.assert_not_called()

    @mock.patch('requests.post')
    @mock.patch('app.decrypt_azure_key_with_kms_key')
    def test_call_face_api_post_request_fails(self, mock_decrypt_function, mock_requests_post):

        # set up mocks
        mock_response = mock.Mock()
        mock_response.json.return_value = testdata.mock_response_from_face_api
        mock_requests_post.return_value = mock_response
        mock_decrypt_function.return_value = 'dummystring'

        # setup fake data to send to function
        imageurl = 'https://garbageurl.com/upload/nothing.jpg'

        # call the function to test
        app.call_face_api(imageurl)

        # verifications
        mock_response.json.assert_called_once()
