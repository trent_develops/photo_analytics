mock_response_from_face_api = [
  {
    "faceId": "4e5025c4-0cb3-4e5a-a946-bc6cd32fe529",
    "faceRectangle": {
      "top": 782,
      "left": 1140,
      "width": 1319,
      "height": 1319
    },
    "faceAttributes": {
      "smile": 1.0,
      "headPose": { "pitch": 0.0, "roll": -10.0, "yaw": -7.7 },
      "gender": "male",
      "age": 4.0,
      "facialHair": { "moustache": 0.0, "beard": 0.0, "sideburns": 0.0 },
      "glasses": "NoGlasses",
      "emotion": {
        "anger": 0.0,
        "contempt": 0.0,
        "disgust": 0.0,
        "fear": 0.0,
        "happiness": 1.0,
        "neutral": 0.0,
        "sadness": 0.0,
        "surprise": 0.0
      },
      "blur": { "blurLevel": "medium", "value": 0.3 },
      "exposure": { "exposureLevel": "goodExposure", "value": 0.68 },
      "noise": { "noiseLevel": "high", "value": 1.0 },
      "makeup": { "eyeMakeup": False, "lipMakeup": False },
      "accessories": [],
      "occlusion": {
        "foreheadOccluded": False,
        "eyeOccluded": False,
        "mouthOccluded": False
      },
      "hair": {
        "bald": 0.08,
        "invisible": False,
        "hairColor": [
          { "color": "blond", "confidence": 1.0 },
          { "color": "gray", "confidence": 0.77 },
          { "color": "other", "confidence": 0.34 },
          { "color": "red", "confidence": 0.27 },
          { "color": "brown", "confidence": 0.2 },
          { "color": "black", "confidence": 0.01 }
        ]
      }
    }
  }
]

mock_incoming_event_from_s3 = {
  'Records':[
    {
      's3':{
        'bucket':{
          'name':'dummyBucketName'
        },
        'object':{
          'key':'dummyKey'
        }
      }
    }
  ]
}
